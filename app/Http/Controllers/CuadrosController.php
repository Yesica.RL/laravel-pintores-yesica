<?php

namespace App\Http\Controllers;
use App\Cuadro;
use App\Pintor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CuadrosController extends Controller
{
    public function getCrear()
    {
    	$pintores=Pintor::all();
    	return view('cuadros.crear',array('pintores'=>$pintores));
    }
    public function postCrear(Request $request)
    {
    	$cuadro= new Cuadro();
    	$cuadro->pintor_id=$request->pintor;
		$cuadro->nombre=$request->nombre;
    	$cuadro->imagen=$request->imagen->store('','cuadros');
    	try{
    		$cuadro->save();
    		return redirect('pintores')->with('mensaje','cuadro:'. $cuadro->nombre .'guardada');
    	}catch(\Illuminate\Database\QueryException $ex){
    		

    	}
    	

    	return redirect('pintores');
    }
}
