@extends('layouts.master')
@section('titulo')
 Crear Cuadro
@endsection
@section('contenido')
	<div class="row">
	 <div class="offset-md-3 col-md-6">
		 <div class="card">
			 <div class="card-header text-center">
			 	Añadir mascota
			 </div>
			 <div class="card-body" style="padding:30px">
			 	<form method="post" action="{{action('CuadrosController@postCrear')}}" enctype="multipart/form-data">
			 		{{ csrf_field() }}
					 <div class="form-group">
						 <label for="nombre">Nombre</label>
						 <input type="text" name="nombre" id="nombre" class="form-control">
					 </div>
					 <div class="form-group">
					 	<label for='pintor'>Pintor</label>
					 	<select class="form-control" name="pintor">
					 		@foreach ($pintores as $pintor)
					 			<option value="{{$pintor->id}}">
					 				{{$pintor->nombre}}
					 			</option>
					 		@endforeach
					 	</select>
					 </div>
					 <div class="form-group">
					 	<input type="file" name="imagen" id="imagen" >
					 </div>
					 <div class="form-group text-center">
						 <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
						 	Crear Cuadro
						 </button>
					 </div>
				 </form>
			 </div>
		 </div>
	 </div>
	</div>
@endsection