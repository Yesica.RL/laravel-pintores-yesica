@extends('layouts.master')
@section('titulo')
	Todos Los Pintores
@endsection
@section('contenido')
	<table>
		<tr>
			<td>Nombre</td>
			<td>Pais</td>
			<td>Cuadros</td>
		</tr>
		
			@foreach ($pintores as $pintor)
				<tr>
					<td><a href="{{url('pintores/mostrar')}}/{{$pintor->id}}">{{$pintor->nombre}}</a></td>
					<td>{{$pintor->pais}}</td>
					<td>{{count($pintor->cuadros)}}</td>
				</tr>
			@endforeach
		
	</table>
@endsection