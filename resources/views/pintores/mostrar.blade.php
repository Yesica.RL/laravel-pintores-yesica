@extends('layouts.master')
@section('titulo')
	Mostrar pintor
@endsection
@section('contenido')
	<div class="col-sm-9">
		<h2>{{$pintor->nombre}}</h2><br>
		<h3>Pais:{{$pintor->pais}}</h3>
		<br>
		<h3>Cuadros</h3>
		@foreach ($pintor->cuadros as $cuadro)
			<div class="col-xs-12 col-sm-6 col-md-4 ">
				<h4 style="min-height:45px;margin:5px 0 10px 0">
						{{$cuadro->nombre}}
					</h4>
				<img src="{{ asset('assets/imagenes') }}/{{$cuadro->imagen}}" style="height:200px"/>
			</div>
		@endforeach
		
		<a href="{{url('/')}}" class="btn btn-danger">Volver al listado</a>
	</div>
@endsection